Validator = require("./utils/validator")

class DFAGenerator
  validator = new Validator()
  constructor: (@setOfStates, @setOfAlphabets, @transitionFunction, @initialState, @finalStates) ->

  _getFinalState: (inputString, initialState, transitionFunction) ->
    inputString.split("").reduce((state, alphabet) ->
      transitionFunction[state][alphabet]
    , initialState)

  getAutomata: () ->
    _parent = @
    return (inputString) ->
      throw "Invalid Input" unless validator.validate(inputString, _parent.setOfAlphabets)
      throw "Invalid Transition Function" unless validator.isValidTransitionFunction(_parent.setOfStates, _parent.transitionFunction)
      throw "Invalid Initial State" unless validator.isValidInitialState(_parent.setOfStates, _parent.initialState)
      throw "Invalid Final State" unless validator.isValidFinalState(_parent.setOfStates, _parent.finalStates)

      finalStatesForInputString = _parent._getFinalState(inputString, _parent.initialState, _parent.transitionFunction)
      _parent.finalStates.indexOf(finalStatesForInputString) >= 0




module.exports = DFAGenerator
