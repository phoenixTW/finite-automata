class Set
  _zero = 0
  union: (collection) ->
    collection.reduce((previousElement, currentElement) ->
      previousElement.push(currentElement) if previousElement.indexOf(currentElement) < 0
      previousElement
    , [])

  isSubset: (candidate, superSet) ->
    candidate.every((element) ->
      superSet.indexOf(element) >= 0
    )
    



module.exports = Set