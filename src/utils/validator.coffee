Set = require("./set")

class Validator
  set = new Set()
  validate: (inputString, setOfAlphabets) ->
    uniqueAlphabets = set.union(inputString.split(""))
    set.isSubset(uniqueAlphabets, setOfAlphabets)

  isValidTransitionFunction: (setOfStates, transitionFunction) ->
    set.isSubset(Object.keys(transitionFunction), setOfStates)

  isValidFinalState: (setOfStates, finalStates) ->
    set.isSubset(finalStates, setOfStates)

  isValidInitialState: (setOfStates, initialState) ->
    set.isSubset(initialState.split(" "), setOfStates)


module.exports = Validator