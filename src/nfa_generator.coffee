Validator = require("./utils/validator")
_ = require("lodash")

class NFAGenerator
  validator = new Validator()
  epsilon = "e"
  _parent = null

  constructor: (@setOfStates, @setOfAlphabets, @transitionFunction, @initialState, @finalStates) ->
    _parent = @

  _epsilionTransitionLookup: (state) ->
    nextStateWithEpsilon = (_parent.transitionFunction[state] and _parent.transitionFunction[state][epsilon]) or []
    _.flatten(nextStateWithEpsilon.concat(nextStateWithEpsilon.map(_parent._epsilionTransitionLookup)))

  _getFinalState: (inputString, initialState, transitionFunction) ->
    return _parent._epsilionTransitionLookup(initialState) if(inputString.length is 0)

    inputString.split("").reduce((states, alphabet) ->
      transitionMethods = {}

      transitionMethods._getTransitionsFor = (state) ->
        alphabetTransitions = _parent.transitionFunction[state][alphabet] or []
        epsilonTransitions = _parent._epsilionTransitionLookup(state)
        _.flatten(epsilonTransitions.map(transitionMethods._getTransitionsFor)
        .concat(alphabetTransitions.map(_parent._epsilionTransitionLookup))
        .concat(alphabetTransitions)
        )

      _.flatten states.map(transitionMethods._getTransitionsFor)
    , [initialState])

  getAutomata: () ->
    return (inputString) ->
      throw "Invalid Input" unless validator.validate(inputString, _parent.setOfAlphabets)
      throw "Invalid Transition Function" unless validator.isValidTransitionFunction(_parent.setOfStates, _parent.transitionFunction)
      throw "Invalid Initial State" unless validator.isValidInitialState(_parent.setOfStates, _parent.initialState)
      throw "Invalid Final State" unless validator.isValidFinalState(_parent.setOfStates, _parent.finalStates)

      finalStatesForInputString = _parent._getFinalState(inputString, _parent.initialState, _parent.transitionFunction)
      not _.isEmpty _.intersection(_parent.finalStates, finalStatesForInputString)


module.exports = NFAGenerator