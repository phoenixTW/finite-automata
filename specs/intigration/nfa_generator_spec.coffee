assert = require('chai').assert
NFAGenerator = require('../../src/nfa_generator')
tuplesCollection = require('../resources/nfa_tuples.json')

describe("NFAGenerator", () ->
  describe("w ∈ Σ∗ | w ends with 00", () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.endWith00
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for 100", () ->
      assert.isTrue(automata("100"))
      assert.isFalse(automata("1001"))
    )
    it("should pass for 111010100", () ->
      assert.isTrue(automata("111010100"))
    )
  )

  describe("w ∈ Σ∗00Σ∗ | w contains 00", () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.havingSubstring00or11
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for contains 00", () ->
      assert.isTrue(automata("1001"))
      assert.isTrue(automata("10001"))
      assert.isTrue(automata("100101"))
      assert.isTrue(automata("100"))
    )
    it("should fail for non 00", () ->
      assert.isFalse(automata("1010"))
    )
  )
  describe("w ∈ Σ∗11Σ∗ | w contains 11", () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.havingSubstring00or11
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for contains 11", () ->
      assert.isTrue(automata("11"))
      assert.isTrue(automata("11001"))
      assert.isTrue(automata("1001011"))
    )
    it("should fail for non 11", () ->
      assert.isFalse(automata("1010"))
    )
    it("should throw 'Invalid Input' exception for 0121", () ->
      exceptionCallback = () -> automata("0211")
      assert.throws(exceptionCallback, "Invalid Input")
    )

    it("should throw 'Invalid Final State' exception", () ->
      tuples = tuplesCollection.inValidFinalStateTuples
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
      exceptionCallback = () -> automata("100")
      assert.throws(exceptionCallback, "Invalid Final State")
    )

    it("should throw 'Invalid Transition Function' exception", () ->
      tuples = tuplesCollection.invalidTransitionFunctionTuples
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
      exceptionCallback = () -> automata("011")
      assert.throws(exceptionCallback, "Invalid Transition Function")
    )

    it("should throw 'Invalid Initial State' exception", () ->
      tuples = tuplesCollection.invalidInitialStateTuple
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
      exceptionCallback = () -> automata("011")
      assert.throws(exceptionCallback, "Invalid Initial State")
    )
  )
  
  describe("language having two epsilon", () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.twoEplisilon0
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should accept 0", () ->
        assert.isTrue(automata("0"))
    )
    it("should not accept 1", () ->
        assert.isFalse(automata("1"))
    )
    it("should not accept empty string", () ->
        assert.isFalse(automata(""))
    )
  )

  describe("Language ends with even number of 1s", () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.endWithEvenNumberOf1s
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for 11", () ->
        assert.isTrue(automata("11"))
    )
    it("should pass for 011", () ->
        assert.isTrue(automata("011"))
    )
    it("should not pass for 0111", () ->
        assert.isFalse(automata("0111"))
    )
  )

  describe("language with length 2 and 3", () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.endWithEpsilon
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for 00", () ->
      assert.isTrue(automata("00"))
    )
  )

  describe 'w ∈ Σ∗0 | w ends with 0', ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.endWith0
      generator = new NFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it 'should accept 110', ->
      assert.isTrue(automata("110"))

    it 'should accept 0', ->
      assert.isTrue(automata("0"))

    it 'should not 11', ->
      assert.isFalse(automata("11"))

)
