assert = require('chai').assert
DFAGenerator = require('../../src/dfa_generator')
tuplesCollection = require('../resources/dfa_tuples.json')

describe('DFAGenerator', () ->
  describe('Language Σ* | where Σ = { 0, 1 }', () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.example1
      generator = new DFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for input string 1", () ->
        assert.equal(true, automata("1"))
    )
    it("should pass for input string 101", () ->
      assert.equal(true, automata("101"))
    )
  )

  describe('Language 1Σ* | where Σ = { 0, 1 }', () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.example2
      generator = new DFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for input string 101010", () ->
        assert.equal(true, automata("101010"))
    )
    it("should pass for input string 11111111", () ->
      assert.equal(true, automata("1111111"))
    )
    it("should fail for input string 01111111", () ->
      assert.equal(false, automata("0111111"))
    )

    it("should pass for empty string", () ->
      tuples = tuplesCollection.emptyStringTuples
      generator = new DFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
      assert.equal(true, automata(""))
    )

    it("should throw 'Invalid Input' exception for 0121", () ->
      exceptionCallback = () -> automata("0211")
      assert.throws(exceptionCallback, "Invalid Input")
    )

    it("should throw 'Invalid Transition Function' exception", () ->
      tuples = tuplesCollection.invalidTransitionFunctionTuples
      generator = new DFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
      exceptionCallback = () -> automata("011")
      assert.throws(exceptionCallback, "Invalid Transition Function")
    )

    it("should throw 'Invalid Initial State' exception", () ->
      tuples = tuplesCollection.invalidInitialStateTuple
      generator = new DFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
      exceptionCallback = () -> automata("011")
      assert.throws(exceptionCallback, "Invalid Initial State")
    )
    it("should throw 'Invalid Final State' exception", () ->
      tuples = tuplesCollection.invalidFinalStateTuple
      generator = new DFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
      exceptionCallback = () -> automata("011")
      assert.throws(exceptionCallback, "Invalid Final State")
    )
  )

  describe( "Length divisible by 2 or 3", () ->
    automata = null
    beforeEach(() ->
      tuples = tuplesCollection.divisibleBy2or3
      generator = new DFAGenerator(tuples.setOfStates, tuples.setOfAlphabets, tuples.transitionFunction, tuples.initialState, tuples.finalState)
      automata = generator.getAutomata()
    )

    it("should pass for language length divisible by 2", () ->
      assert.isTrue(automata("1010"))
      assert.isFalse(automata("10101"))
    )

    it("should pass for language length divisible by 3", () ->
      assert.isTrue(automata("101"))
      assert.isFalse(automata("1"))
      assert.isFalse(automata("1110010"))
    )
  )
)


