gulp = require('gulp')
gutil = require('gulp-util')
del = require('del')
_ = require('lodash')
coffee = require('gulp-coffee')
changed = require('gulp-changed')
batch = require('gulp-batch')
run = require('gulp-run')

SRC =
  src:
    root: 'src'
    coffee: 'src/**/*.coffee'
    json: 'src/**/*.json'
  test:
    coffee: 'specs/**/*_spec.coffee'
    resources: 'specs/**/*.json'


TASKS =
  scripts: '_scripts'
  clean: 'clean'
  build: 'build'
  runTest: 'test'
  compileTest: 'compile-test'
  watch: 'watch'
  test_resources: "_test_resources"
  copyTestJSON: '_copyTestJSON'

DEST =
  build: 'build'
  src: 'build/src'
  test: 'build/specs'


gulp.task TASKS.runTest, [
  TASKS.scripts,
  TASKS.test_resources
], () ->
  gulp.src(SRC.test.coffee).pipe(changed(DEST.test, extension: '.js')).pipe(coffee()).on('error', gutil.log).pipe(gulp.dest(DEST.test)).pipe run('mocha build/specs/**/*.js')

gulp.task TASKS.compileTest, [
  TASKS.scripts,
  TASKS.test_resources
], () ->
  gulp.src(SRC.test.coffee).pipe(changed(DEST.test, extension: '.js')).pipe(coffee()).on('error', gutil.log).pipe(gulp.dest(DEST.test))

gulp.task TASKS.clean, (cb) ->
  del DEST.build, cb

gulp.task TASKS.scripts, () ->
  gulp.src(SRC.src.coffee).pipe(changed(DEST.src, extension: '.js')).pipe(coffee()).on('error', gutil.log).pipe(gulp.dest(DEST.src))

gulp.task TASKS.build, [
  TASKS.scripts
], () ->
  return

gulp.task TASKS.watch, [
  TASKS.scripts
], (cb) ->
  gulp.watch SRC.src.coffee, [ TASKS.scripts ]


gulp.task TASKS.test_resources, () ->
  gulp.src(SRC.test.resources).pipe(changed(DEST.test)).pipe(gulp.dest(DEST.test))